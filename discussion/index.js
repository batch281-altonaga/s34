// Use the "require" directive to load the express package
const express = require("express");

// Create a new instance of express
const app = express();

const port = 4000;

app.use(express.json());

// app.use is a middleware function that will be called on every request
// express.urlencoded can be used to parse the request body
// extended: true means that the values in the request body can be arrays or objects
app.use(express.urlencoded({ extended: true }));

// app.get is a route handler that will be called when a GET request is made to the root path
app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.get("/hello", (req, res) => {
  res.send("Hello from the /hello endpoint!");
});

app.post("/hello", (req, res) => {
  res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
});

// mock database
let users = [];

app.post("/signup", (req, res) => {
  console.log(req.body);

  if (req.body.username !== undefined && req.body.password !== undefined) {
    users.push(req.body);

    res.send("User created!");
  } else {
    res.status(400).send("Bad request");
  }
});

app.put("/changepassword", (req, res) => {
  let message;

  for (let i = 0; i < users.length; i++) {
    if (users[i].username === req.body.username) {
      users[i].password = req.body.password;
      message = "Password changed!";
      break;
    } else {
      message = "User not found!";
    }
  }
  res.send(message);

  console.log(message);
});

// If (require.main) allows us to listen to the app directly
if (require.main === module) {
  app.listen(port, () => {
    console.log(`Listening on port ${port}`);
  });
}

module.exports = app;
